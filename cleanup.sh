rm -rf ~/Library/Developer/Xcode/Archives/*
rm -rf ~/Library/Developer/Xcode/DerivedData/*
rm -rf ~/Library/Caches/*
rm -rf ~/.Trash/*
rm -rf ~/Library/Developer/CoreSimulator/Caches/*
rm -rf ~/Library/Application Support/MobileSync/Backup
rm -rf ~/Library/Caches/com.apple.dt.Xcode
xcrun simctl delete unavailable
